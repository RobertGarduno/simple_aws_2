package main

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
)

const (
	id  = "XXXXXXXXXXXXXXXXXXXXX"
	key = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
)

func main() {
	s, _ := session.NewSession(&aws.Config{
		Credentials: credentials.NewStaticCredentials(id, key, "")})
	fmt.Print(s.Config)
}
